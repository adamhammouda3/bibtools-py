Author(s):           Adam Hammouda
Last Revision Date:  17 September 2013

A simple python utility for sorting bibtex files by some key.
[Currently this 'key' is implicitly the id-slug of the bibitem].

I know there are better tools out there for playing with bibtex files,
but I wanted something easily hackable in python.